import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">
      Created By MWP1 - Justin Lai, Mariah Janet Lindsa and Mustafa Khawaja
    </span>
  `,
})
export class FooterComponent {
}
