import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'ngx-form-layouts',
  styleUrls: ['./form-layouts.component.scss'],
  templateUrl: './form-layouts.component.html',
})
export class FormLayoutsComponent {
  newUserForm: FormGroup;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };

  constructor(
    private fb: FormBuilder,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.newUserForm = this.fb.group({
      price: ['', Validators.required],
      stockTicker: ['', Validators.required],
      stockQuantity: ['', Validators.required]
    });
  }

  onSubmit() {
    var newprice = this.newUserForm.get('price').value;
    var newstockQuantity = this.newUserForm.get('stockQuantity').value;
    var stockTicker = this.newUserForm.get('stockTicker').value;
    var stockData = {
      "askingPrice" : newprice,
      "quantity" : newstockQuantity,
      "stockTicker" : stockTicker
    }
    var newUserData = {
      "stock" : stockData
    }
    this.http.post('http://localhost:8080/users', newUserData, this.httpOptions).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    )
    this.newUserForm.reset();
  }
}
