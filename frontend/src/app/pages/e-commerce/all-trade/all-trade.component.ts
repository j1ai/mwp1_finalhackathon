import { Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';
import { FormBuilder } from '@angular/forms';
import { AllTradeData, AllTrade, Stock } from '../../../@core/data/all-trade';
import { AllTradeService } from 'app/@core/mock/all-trade.service';
import {NgForm} from '@angular/forms';


@Component({
  selector: 'ngx-all-trade',
  styleUrls: ['./all-trade.component.scss'],
  templateUrl: './all-trade.component.html',
})

export class ECommerceAllTradeComponent implements OnDestroy {
  private alive = true;
  alltrades: Stock[] = [];
  allUsersIds: string[] = [];
  curUser = 'All Users';
  currentTheme: string;

  constructor(private themeService: NbThemeService,
              private alltradeService: AllTradeService) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.currentTheme = theme.name;
    });
    this.getAllTrade();
  }

  getAllTrade() {
    this.alltradeService.getAllTradeData()
      .subscribe(alltradeData => {
        let allusers = alltradeData;
        this.allUsersIds.length = 0;
        this.alltrades.length = 0;
        for (let user of allusers){
          this.allUsersIds.push(user.userID);
          for (let stock of user.history){
            this.alltrades.push(stock);
          }
        }
        this.allUsersIds.push('All Users');
      });
  }



  getUserAllTrade(userID: string){
    this.alltrades.length = 0;
    if(userID == 'All Users'){
      this.getAllTrade();
    }
    else{
      this.alltradeService.getUserTradeData(userID)
      .subscribe(alltradeData => {
        let userTradeData = alltradeData;
        for (let stock of userTradeData.history){
          this.alltrades.push(stock);
        }
      });
    }

  }



  ngOnInit(): void {
    this.getAllTrade();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
