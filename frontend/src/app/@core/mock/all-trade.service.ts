import { Injectable } from '@angular/core';
import { of as observableOf,  Observable , throwError} from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { AllTrade, AllTradeData} from '../data/all-trade';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AllTradeService extends AllTradeData {

  apiUrl: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };
  constructor(private http: HttpClient) {
    super();
    this.apiUrl = 'http://localhost:8080/users';
  }

  

/*   constructor(private periods: PeriodsService) {
    super();
    this.data = {
      // week: this.getDataWeek(),
      // month: this.getDataMonth(),
      // year: this.getDataYear(),
    };
  } */

  // private getDataWeek(): AllTrade[] {
  //   // return this.periods.getWeeks().map((week) => {
  //   //   return this.generateAllTradeRandomData(week);
  //   // });
  // }

  // private getDataMonth(): AllTrade[] {
  //   const currentDate = new Date();
  //   const days = currentDate.getDate();
  //   const month = this.periods.getMonths()[currentDate.getMonth()];

  //   // return Array.from(Array(days)).map((_, index) => {
  //   //   const date = `${index + 1} ${month}`;

  //   //   return this.generateAllTradeRandomData(date);
  //   // });
  // }

  // private getDataYear(): AllTrade[] {
  //   // return this.periods.getYears().map((year) => {
  //   //   return this.generateAllTradeRandomData(year);
  //   // });
  // }

  getAllTradeData(): Observable<any> {
    return this.http.get<AllTrade[]>(this.apiUrl)
    .pipe(
      retry(1),
      catchError(this.errorHandler)
    );
  }

  getUserTradeData(userID: string): Observable<any> {
    return this.http.get<AllTrade[]>(this.apiUrl + "/" + userID)
    .pipe(
      retry(1),
      catchError(this.errorHandler)
    );
  }

  errorHandler(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
