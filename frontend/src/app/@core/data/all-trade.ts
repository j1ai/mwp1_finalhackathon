import { Observable } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';

export interface Stock {
  createDate: string;
  tradeID: string,
  quantity: number;
  askingPrice: number;
  stockTicker: string;
  state: string;
}

export interface AllTrade {
  stock: Stock;
  history : Stock[];
  userID: string;
}

export abstract class AllTradeData {
  abstract getAllTradeData(): Observable<AllTrade[]>;
}
