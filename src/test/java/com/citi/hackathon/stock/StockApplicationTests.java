package com.citi.hackathon.stock;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.citi.hackathon.stock.entities.Stock;
import com.citi.hackathon.stock.repository.StockRepo;
import com.citi.hackathon.stock.service.StockService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(classes={com.citi.hackathon.stock.StockApplication.class})
@TestPropertySource(locations = "classpath:application-test.properties")
class StockApplicationTests {

	@Autowired
	private StockRepo repo;

	@Autowired
	private StockService service;



	//TODO: Make this work
	// @Test

	// void canAddStock(){
	// 	Stock testStock = new Stock(); // fields will be null
	// 	int numOfStocks = repo.findAll().size();
	// 	service.addStock(testStock);
	// 	assertTrue(numOfStocks> repo.findAll().size());
	// }

	@Test
	void contextLoads() {
	}



}
