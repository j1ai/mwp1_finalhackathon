package com.citi.hackathon.stock.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

import java.util.List;

import com.citi.hackathon.stock.entities.User;

import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;


public class UserControllerFunctionalTest {

    private RestTemplate template = new RestTemplate();

    @Test
    public void testFindAll(){
        @SuppressWarnings("unchecked")
        List<User> users = template.getForObject("http://localhost:8080/users", List.class);
    
        assertThat(users, notNullValue());
    }


}
