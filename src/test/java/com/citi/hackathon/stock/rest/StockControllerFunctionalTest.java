package com.citi.hackathon.stock.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

import java.util.List;

import com.citi.hackathon.stock.entities.Stock;

import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

public class StockControllerFunctionalTest {

    private RestTemplate template = new RestTemplate();

    @Test
    public void testFindAll(){
        @SuppressWarnings("unchecked")
        List<Stock> stocks = template.getForObject("http://localhost:8080/stock", List.class);
    
        assertThat(stocks, notNullValue());
    }

    @Test
    public void testStockById() {
        Stock stock = template.getForObject
            ("http://localhost:8080/stock/5f5bcd4e435ba012dcb11102", Stock.class);
        assertThat(stock, notNullValue());
    }
    
}