package com.citi.hackathon.stock.rest;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.citi.hackathon.stock.controller.UserController;
import com.citi.hackathon.stock.entities.Stock;
import com.citi.hackathon.stock.entities.User;
import com.citi.hackathon.stock.service.UserService;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserControllerUnitTestNoSpring {
    

    public static final String TEST_ID = "5f46a3d545bee629d17fd7b2";

    private UserController controller;
    
    @BeforeEach
    public void setUp() {
        ObjectId ID = new ObjectId(TEST_ID);
        Stock stock1 = new Stock(321, 100, "TESTER1");
        Stock stock2 = new Stock(123, 10, "TESTER2");
        User user1 = new User(stock1);
        User user2 = new User(stock2);
        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        UserService service = mock(UserService.class);
        when(service.getUsers()).thenReturn(users);

        controller = new UserController(service);
        String firstID = user1.getUserID();
        String secondID = user1.getUserID();
    }

    @Test
    public void testFindAll() {
        Iterable<User> CDs = controller.findAll();
        Stream<User> stream = StreamSupport.stream(CDs.spliterator(), false);
        assertThat(stream.count(), equalTo(2L));
    }


    
}
