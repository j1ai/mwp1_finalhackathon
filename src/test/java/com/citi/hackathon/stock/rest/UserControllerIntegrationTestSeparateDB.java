package com.citi.hackathon.stock.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.citi.hackathon.stock.controller.UserController;
import com.citi.hackathon.stock.entities.Stock;
import com.citi.hackathon.stock.entities.User;
import com.citi.hackathon.stock.repository.UserRepo;
import com.citi.hackathon.stock.service.UserService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = UserControllerIntegrationTestSeparateDB.Config.class)
public class UserControllerIntegrationTestSeparateDB {

    @SpringBootApplication(scanBasePackageClasses = { UserController.class, UserService.class })
    @EnableMongoRepositories(basePackages = "com.citi.hackathon.stock")
    @PropertySource("file:src/test/java/com/citi/hackathon/stock/resources/test.properties")
    public static class Config {
    }

    @Autowired
    private UserController controller;

    @Autowired
    private UserRepo repo;

    public String testID;

    @BeforeEach
    public void setUp() {
        repo.deleteAll();

        Stock testStock = new Stock(2, 100, "TESTER");
        User testUser = new User(testStock);
        repo.save(testUser);
    }
    
    @Test
    public void testFindAll() {
        Iterable<User> Users = controller.findAll();
        Stream<User> stream = StreamSupport.stream(Users.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }

    @Test
    public void testUserAddUpdateDelete(){
        //Test addUser:
        Stock stock = new Stock(321, 100, "TESTER1");
        User test = new User(stock);
        controller.addUser(test);
        String innerTestID = test.getUserID();
        System.out.println(innerTestID);
        //Assert user added
        Iterable<User> Users = controller.findAll();
        Stream<User> stream = StreamSupport.stream(Users.spliterator(), false);
        assertThat(stream.count(), equalTo(2L));

        //Test updateUser
        Stock stock2 = new Stock(123, 1, "TESTER2");
        test.setStock(stock2);
        controller.updateUser(test, innerTestID);
        //Add fetch check that it changed and assert size is the same
        Optional<User> check = controller.getUserByUserID(innerTestID);
        //TODO: Fix this fetch test...
        //Stock testCheck = check.getStock();
        Iterable<User> Users2 = controller.findAll();
        Stream<User> stream2 = StreamSupport.stream(Users2.spliterator(), false);
        assertThat(stream2.count(), equalTo(2L));
        


        //Test deleteUser
        controller.deleteUser(innerTestID);
        //assert that user was deleted
        Iterable<User> Users3 = controller.findAll();
        Stream<User> stream3 = StreamSupport.stream(Users3.spliterator(), false);
        assertThat(stream3.count(), equalTo(1L));
    }
    

    
}
