package com.citi.hackathon.stock.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.notNullValue;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.citi.hackathon.stock.StockApplication;
import com.citi.hackathon.stock.controller.UserController;
import com.citi.hackathon.stock.entities.Stock;
import com.citi.hackathon.stock.entities.User;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest(classes={com.citi.hackathon.stock.StockApplication.class})
@ContextConfiguration(classes=StockApplication.class)
public class UserControllerIntegrationTest {
    
    @Autowired
    private UserController controller;

    @Test
    public void testAddUser(){
        Stock stock = new Stock(2, 100, "TESTER0");
        User test = new User(stock);
        controller.addUser(test);
    }

    @Test
    public void testFindAll() {
        Iterable<User> users = controller.findAll();
        Stream<User> stream = StreamSupport.stream(users.spliterator(), false);
        assertThat(stream.count(), greaterThanOrEqualTo(1L));
    }


    @Test
    public void testUserAddUpdateDelete(){
        //Test addUser:
        Stock stock = new Stock(321, 100, "TESTER1");
        User test = new User(stock);
        controller.addUser(test);
        String innerTestID = test.getUserID();
        System.out.println(innerTestID);
        //Assert User Added:
        Iterable<User> users2 = controller.findAll();
        Stream<User> stream2 = StreamSupport.stream(users2.spliterator(), false);
        assertThat(stream2.count(), greaterThanOrEqualTo(2L));

        //Test updateUser
        Stock stock2 = new Stock(123, 1, "TESTER2");
        test.setStock(stock2);
        controller.updateUser(test, innerTestID);

        //Test deleteUser
        controller.deleteUser(innerTestID);
        Iterable<User> Users3 = controller.findAll();
        Stream<User> stream3 = StreamSupport.stream(Users3.spliterator(), false);
        assertThat(stream3.count(), greaterThanOrEqualTo(1L));
    }





    
}
