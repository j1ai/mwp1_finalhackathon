package com.citi.hackathon.stock.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.citi.hackathon.stock.StockApplication;
import com.citi.hackathon.stock.controller.StockController;
import com.citi.hackathon.stock.entities.Stock;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest(classes={com.citi.hackathon.stock.StockApplication.class})
@ContextConfiguration(classes=StockApplication.class)
public class StockControllerIntegrationTest {

    public static final String TEST_ID = "5f5bcd4e435ba012dcb11102";

    @Autowired
    private StockController controller;

    @Test
    public void testFindAll() {
        Iterable<Stock> stocks = controller.findAll();
        Stream<Stock> stream = StreamSupport.stream(stocks.spliterator(), false);
        assertThat(stream.count(), notNullValue());
    }
    
    @Test
    public void testStockById() {
        Optional<Stock> stock = controller.getStockById(TEST_ID);
        assertThat(stock.isPresent(), equalTo(true));
    }
    
}