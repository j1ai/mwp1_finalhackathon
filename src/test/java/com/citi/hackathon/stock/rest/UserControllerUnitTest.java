package com.citi.hackathon.stock.rest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.citi.hackathon.stock.controller.UserController;
import com.citi.hackathon.stock.entities.Stock;
import com.citi.hackathon.stock.entities.User;
import com.citi.hackathon.stock.repository.UserRepo;
import com.citi.hackathon.stock.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.notNullValue;

@ExtendWith(SpringExtension.class)
//@SpringBootTest(classes={com.citi.hackathon.stock.StockApplication.class})
@ContextConfiguration(classes=UserControllerUnitTest.Config.class)
public class UserControllerUnitTest {

    public static final String TEST_ID = "5f6110b87000040555500ece";

    @Configuration
    public static class Config {

        @Bean
        public UserRepo repo() {
            return mock(UserRepo.class);
        }

        @Bean
        public UserService service() {

            //ObjectId ID = new ObjectId(TEST_ID);
            Stock stock = new Stock(2, 100, "9999");
		    User user = new User(stock);

            List<User> users = new ArrayList<>();
            users.add(user);

            UserService service = mock(UserService.class);
            when(service.getUsers()).thenReturn(users);
            //when(service.getUserById(ID)).thenReturn(Optional.of(user));
            return service;
        }

        @Bean
        public UserController controller() {
            return new UserController();
        }
    }

    @Autowired
    private UserController controller;
    
    @Test
    public void testFindAll() {
        Iterable<User> users = controller.findAll();
        Stream<User> stream = StreamSupport.stream(users.spliterator(), false);
        assertThat(stream.count(), notNullValue());
    }
    
}
