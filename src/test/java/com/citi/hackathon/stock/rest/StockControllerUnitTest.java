package com.citi.hackathon.stock.rest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.citi.hackathon.stock.controller.StockController;
import com.citi.hackathon.stock.entities.Stock;
import com.citi.hackathon.stock.repository.StockRepo;
import com.citi.hackathon.stock.service.StockService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.*;

@ExtendWith(SpringExtension.class)
//@SpringBootTest(classes={com.citi.hackathon.stock.StockApplication.class})
@ContextConfiguration(classes=StockControllerUnitTest.Config.class)
public class StockControllerUnitTest {

    public static final String TEST_ID = "5f46a3d545bee629d17fd7b2";

    @Configuration
    public static class Config {

        // We really shouldn't need this, but when Spring tries to configure a
        // CompactDiscService bean -- even a mock one! it insists on resolving
        // the @Autowired dependency to a CompactDiscRepository.
        @Bean
        public StockRepo repo() {
            return mock(StockRepo.class);
        }

        @Bean
        public StockService service() {

            ObjectId ID = new ObjectId(TEST_ID);
            Stock stock = new Stock(2, 100, "9999");
            List<Stock> stocks = new ArrayList<>();
            stocks.add(stock);

            StockService service = mock(StockService.class);
            when(service.getStocks()).thenReturn(stocks);
            when(service.getStockById(ID)).thenReturn(Optional.of(stock));
            return service;
        }

        @Bean
        public StockController controller() {
            return new StockController();
        }
    }

    @Autowired
    private StockController controller;
    
    @Test
    public void testFindAll() {
        Iterable<Stock> stocks = controller.findAll();
        Stream<Stock> stream = StreamSupport.stream(stocks.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }
    
    @Test
    public void testStockById() {
        Optional<Stock> stock = controller.getStockById(TEST_ID);
        assertThat(stock.isPresent(), equalTo(true));
    }
}