package com.citi.hackathon.stock;

import static org.junit.jupiter.api.Assertions.assertTrue;


import com.citi.hackathon.stock.repository.UserRepo;
import com.citi.hackathon.stock.service.UserService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(classes={com.citi.hackathon.stock.StockApplication.class})
@TestPropertySource(locations = "classpath:application-test.properties")
public class UserApplicationTests {

    @Autowired
	private UserRepo repo;

	@Autowired
	private UserService service;

    @Test
	void contextLoads() {
	}
    
}
