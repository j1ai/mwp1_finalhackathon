package com.citi.hackathon.stock.repository;

import com.citi.hackathon.stock.entities.User;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepo extends MongoRepository<User, String> {
    
}
