package com.citi.hackathon.stock.repository;

import java.util.List;

import com.citi.hackathon.stock.entities.Stock;
import com.citi.hackathon.stock.entities.StockState;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockRepo extends MongoRepository<Stock, ObjectId> {
    //public List<Stock> findByState(StockState state);
    
}