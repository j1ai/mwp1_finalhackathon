package com.citi.hackathon.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import java.util.Collections;

import com.citi.hackathon.stock.entities.Stock;
import com.citi.hackathon.stock.entities.User;
import com.citi.hackathon.stock.repository.UserRepo;

@SpringBootApplication
@EnableMongoRepositories(basePackages = "com.citi.hackathon.stock.repository")
public class StockApplication {

	public static void main(String[] args) {
        SpringApplication.run(StockApplication.class, args);
        
        //ToDo: Add in Demo Users
        // ApplicationContext context = SpringApplication.run(StockApplication.class, args);
		// UserRepo tester = context.getBean(UserRepo.class);

		// Stock stock = new Stock(2, 100, "9999");
		// User user = new User(stock);
		// tester.save(user);

	}

	@Bean
    public FilterRegistrationBean<CorsFilter> simpleCorsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(Collections.singletonList("http://localhost:4200"));
        config.setAllowedMethods(Collections.singletonList("*"));
        config.setAllowedHeaders(Collections.singletonList("*"));
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(source));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }

}
