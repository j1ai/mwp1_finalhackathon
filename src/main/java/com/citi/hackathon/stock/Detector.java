package com.citi.hackathon.stock;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Detector {

	@Value("${spring.data.mongodb.uri:missing}")
	private String uri;

	@Value("${spring.data.mongodb.host:missing}")
	private String host;
	
	@PostConstruct
	public void init() {
		System.out.println("spring.data.mongodb.host=" + host);
		System.out.println("spring.data.mongodb.uri=" + uri);
	}
}


