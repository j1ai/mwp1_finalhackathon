package com.citi.hackathon.stock.entities;

import java.util.ArrayList;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;


import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class User {


    private Stock stock;
    private ArrayList<Stock> tradeHistory = new ArrayList<Stock>();
    // To implement when we have more than one user
    // private String username,password;
    @Id
    private String userID;

    public User(){
        this.userID = UUID.randomUUID().toString().replace("-", "");
    }

    public User(Stock stock){
        this.userID = UUID.randomUUID().toString().replace("-", "");
        this.stock = stock;
        tradeHistory.add(stock);
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
        tradeHistory.add(stock);
    }

    public ArrayList<Stock> getHistory(){
        return this.tradeHistory;
    }

    public String getUserID() {
        return this.userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
    
}
