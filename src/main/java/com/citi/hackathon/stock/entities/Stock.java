package com.citi.hackathon.stock.entities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

@Document
public class Stock {

    @Id
    private ObjectId id;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @CreatedDate
    private Date createDate = new Date();

    //tradeID generator: display stock without having to load each stock in
    // private static final AtomicInteger idGen = new AtomicInteger(100);
    private String tradeID;

    private int quantity;
    private int askingPrice;
    private String stockTicker;
    private StockState state = StockState.CREATED;


    public Stock(int quantity, int askingPrice, String stockTicker) {
        this.tradeID = UUID.randomUUID().toString().replace("-", "");
        this.quantity = quantity;
        this.askingPrice = askingPrice;
        this.stockTicker = stockTicker;
    }
    //Defult constructor: necessary apparently
    public Stock(){
        this.tradeID = UUID.randomUUID().toString().replace("-", "");
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    //Don't need this, keepingit just incase
    public void setCreateDate(String createDate) {
        this.createDate = new Date();
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAskingPrice() {
        return askingPrice;
    }

    public void setAskingPrice(int askingPrice) {
        this.askingPrice = askingPrice;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public String getTradeID() {
        return tradeID;
    }

    public StockState getState() {
        return state;
    }

    public void setState(StockState state) {
        this.state = state;
    }

}
