package com.citi.hackathon.stock.service;

import java.util.Collection;
import java.util.Optional;

import com.citi.hackathon.stock.entities.Stock;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

@Service
public interface StockService {

    Collection<Stock> getStocks();
    void addStock(Stock stock);
    void deleteStock(ObjectId id);
    Optional<Stock> getStockById(ObjectId id);
    
}