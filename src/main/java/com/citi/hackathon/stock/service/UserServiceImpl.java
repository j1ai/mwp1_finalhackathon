package com.citi.hackathon.stock.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import com.citi.hackathon.stock.entities.User;
import com.citi.hackathon.stock.repository.UserRepo;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepo repo;

    @Override 
    public Collection<User> getUsers(){
        return repo.findAll();
    }

    @Override
    public void addUser(User user) {
        repo.insert(user);
    }

    @Override
    public void deleteUser(String UserId) {
        repo.deleteById(UserId);
    }

    @Override
    public Optional<User> getUserById(String UserId) {
        return repo.findById(UserId);
    }

    @Override
    public ArrayList<String> getAllUserIds() {
        ArrayList<String> allUserIds = new ArrayList<String>();
        Collection<User> allUsers = repo.findAll();
        for (User user : allUsers) {
            allUserIds.add(user.getUserID());
        }
        return allUserIds;
    }

    
}
