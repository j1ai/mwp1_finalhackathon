package com.citi.hackathon.stock.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import com.citi.hackathon.stock.entities.User;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    
    Collection<User> getUsers();
    void addUser(User user);
    void deleteUser(String userId);
    Optional<User> getUserById(String userId);
    ArrayList<String> getAllUserIds();
}
