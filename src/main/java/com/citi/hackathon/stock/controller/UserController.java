package com.citi.hackathon.stock.controller;

import java.util.Optional;

import com.citi.hackathon.stock.entities.User;
import com.citi.hackathon.stock.repository.UserRepo;
import com.citi.hackathon.stock.service.UserService;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/users")
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService service;

    @Autowired
    private UserRepo repo;

    public UserController(){}

    public UserController(UserService service){
        this.service = service;
    }
    

    @RequestMapping(method = RequestMethod.GET)
	public Iterable<User> findAll() {
        log.debug("findAll() called in UserController");
		return service.getUsers();
    }

    @RequestMapping(value = "/userIds",method = RequestMethod.GET)
	public Iterable<String> findAllUserIds() {
        log.debug("getAllUserIds() called in UserController");
		return service.getAllUserIds();
    }

    @RequestMapping(method=RequestMethod.POST)
    public void addUser(@RequestBody User user){
        log.debug("addUser() called in UserController");
        service.addUser(user);
    }

    @RequestMapping(method=RequestMethod.DELETE, value = "/{id}")
    public void deleteUser(@PathVariable("id") String UserId){
        log.debug("deleteUser() called in UserController");
        service.deleteUser(UserId);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public User updateUser(@RequestBody User newUser, @PathVariable("id") String userId){
        log.debug("updateUser() called in UserController");
            User oldUser = service.getUserById(userId).orElse(newUser);
            oldUser.setStock(newUser.getStock());

            return repo.save(oldUser);
    }

    @RequestMapping(method=RequestMethod.GET, value = "/{id}")
    public Optional<User> getUserByUserID(@PathVariable("id") String id){
        log.debug("getUserByUserID() called in UserController");
        Optional<User> user = service.getUserById(id);
        if (!user.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
        return user;
    }
}
