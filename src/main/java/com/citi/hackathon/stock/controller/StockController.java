package com.citi.hackathon.stock.controller;

import java.util.Collection;
import java.util.Optional;

import com.citi.hackathon.stock.*;
import com.citi.hackathon.stock.controller.*;
import com.citi.hackathon.stock.entities.Stock;
import com.citi.hackathon.stock.repository.StockRepo;
import com.citi.hackathon.stock.service.StockService;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/stock")
public class StockController {

    private static final Logger log = LoggerFactory.getLogger(StockController.class);

    @Autowired
    private StockService service;

    @Autowired
    private StockRepo repo;

    // @RequestMapping(method=RequestMethod.GET)
    // public Collection<Stock> getStocks(){
    //     return service.getStocks();
        
    // }

    @RequestMapping(method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addStock(@RequestBody Stock stock){
        log.debug("addStock() called in StockController");
        service.addStock(stock);
    }
    
    @RequestMapping(method=RequestMethod.DELETE, value = "/{id}")
    public void deleteStock(@PathVariable("id") String id){
        log.debug("deleteStock() called in StockController");
        service.deleteStock(new ObjectId(""+id));
    }

    @RequestMapping(method=RequestMethod.GET, value = "/{id}")
    public Optional<Stock> getStockById(@PathVariable("id") String id){
        log.debug("getStockById() called in StockController");
        Optional<Stock> stock = service.getStockById(new ObjectId(id));
        if (!stock.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
        return stock;
    }

    @RequestMapping(method = RequestMethod.GET)
	public Iterable<Stock> findAll() {
		log.debug("findAll() called in StockController");
		return service.getStocks();
    }
    

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public Stock updateStock(@RequestBody Stock newStock, @PathVariable("id") ObjectId id){
            log.debug("updateStock() called in StockController");
            Stock oldStock = service.getStockById(id).orElse(newStock);
            oldStock.setAskingPrice(newStock.getAskingPrice());
            oldStock.setQuantity(newStock.getQuantity());
            oldStock.setStockTicker(newStock.getStockTicker());

            return repo.save(oldStock);
    }
}