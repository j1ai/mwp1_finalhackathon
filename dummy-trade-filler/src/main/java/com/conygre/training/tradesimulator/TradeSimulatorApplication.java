package com.conygre.training.tradesimulator;

import com.conygre.training.tradesimulator.dao.TradeMongoDao;
import com.conygre.training.tradesimulator.model.Stock;
import com.conygre.training.tradesimulator.model.StockState;
import com.conygre.training.tradesimulator.model.User;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TradeSimulatorApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(TradeSimulatorApplication.class, args);
		TradeMongoDao tester = context.getBean(TradeMongoDao.class);

		Stock stock = new Stock(2, 100, "9999");
		User user = new User(stock);
		tester.save(user);

	}

}
