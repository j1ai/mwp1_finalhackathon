package com.conygre.training.tradesimulator.model;

public enum StockState {
    CREATED("CREATED"),
    PROCESSING("PROCESSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED");

    private String state;

    private StockState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    } 
    
}
