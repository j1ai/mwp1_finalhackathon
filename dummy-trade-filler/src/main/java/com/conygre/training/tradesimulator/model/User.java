package com.conygre.training.tradesimulator.model;

import java.util.ArrayList;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class User {

    @Id
    private String userId;

    private Stock stock;
    private ArrayList<Stock> tradeHistory = new ArrayList<Stock>();
    // To implement when we have more than one user
    // private String username,password;

    public User(){}

    public User(Stock stock){
        this.stock = stock;
        tradeHistory.add(stock);
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
        tradeHistory.add(stock);
    }

    public StockState getUState() {
        return this.stock.getState();
    }

    public void setUState(StockState state) {
        this.stock.setState(state);
        if(this.stock.getState() == StockState.FILLED || this.stock.getState() == StockState.REJECTED){
            tradeHistory.remove(tradeHistory.size()-1); // Remove the latest in history which is the current stock
            tradeHistory.add(this.stock);
        }
        
    }
    
}
