package com.conygre.training.tradesimulator.sim;

import java.util.List;

import com.conygre.training.tradesimulator.dao.TradeMongoDao;
import com.conygre.training.tradesimulator.model.Stock;
import com.conygre.training.tradesimulator.model.StockState;
import com.conygre.training.tradesimulator.model.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TradeSim {
    private static final Logger LOG = LoggerFactory.getLogger(TradeSim.class);

    @Autowired
    private TradeMongoDao tradeDao;

    @Transactional
    public List<User> findTradesForProcessing(){
        List<User> foundTrades = tradeDao.findByStock_state(StockState.CREATED);
        
        for(User thisTrade: foundTrades) {
            thisTrade.setUState(StockState.PROCESSING);
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Transactional
    public List<User> findTradesForFilling(){
        List<User> foundTrades = tradeDao.findByStock_state(StockState.PROCESSING);

        for(User thisTrade: foundTrades) {
            if((int) (Math.random()*10) > 8) {
                thisTrade.setUState(StockState.REJECTED);
            }
            else {
                thisTrade.setUState(StockState.FILLED);
            }
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Scheduled(fixedRateString = "${scheduleRateMs:10000}")
    public void runSim() {
        LOG.debug("Main loop running!");

        int tradesForFilling = findTradesForFilling().size();
        LOG.debug("Found " + tradesForFilling + " trades to be filled/rejected");

        int tradesForProcessing = findTradesForProcessing().size();
        LOG.debug("Found " + tradesForProcessing + " trades to be processed");

    }
}
