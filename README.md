# MWP1 Final HackaThon: CitiMyTrade #
![](FrontEnd_UI.png)

## MicroService Stock Trading Platform Project ##
This is a stock trading platform comprised of the following features:

* A wed UI that allows users to: request Trades, browse traiding history, and view the status of their historical trades.
* A MongoDB the stores user data such as trades/trade histories.
* A REST API that accepts REST requests to create, update, retrieve, and delete user trade records.


## System Architecture ##

![](Architecture.png)

## What's Unique About Our Project? ##
Our MongoDB stores User entities which conatin a current Trade entity and a Trade History which shows all past trades and their current Trade State.

![](User_entity.PNG)


## How To Run Our Project ##

### REST API ###
Build on the command line with gradel:

```./gradlew build```

Run the built jar with:

```java -jar build/libs/Stock-0.0.1-SNAPSHOT.jar```

with DEBUG logging:

```java -DLOG_LEVEL=DEBUG -DSERVER_PORT=8080 -jar build/libs/Stock-0.0.1-SNAPSHOT.jar```


### WEB UI ###
(You will need to have node already installed)

Navigate to inside the "frontend" folder and run the command:

```npm start```

If this fails try first running 

```npm update```

Then navigate to localhost:4200 in yur browser to view the Frontend

### Dummy-Trader ###
Navigate to inside the "dummy-trade-filler" folder and run the commands:

```./gradlew build```

Run the built jar with:

```java -jar build/libs/trade-simulator-0.0.1-SNAPSHOT.jar```

with DEBUG logging:

```java -DLOG_LEVEL=DEBUG -DSERVER_PORT=8089 -jar build/libs/trade-simulator-0.0.1-SNAPSHOT.jar```